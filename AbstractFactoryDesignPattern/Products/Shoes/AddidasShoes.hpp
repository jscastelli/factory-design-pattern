#ifndef ADDIDASSHOES_HPP
#define ADDIDASSHOES_HPP

#include "Shoes.hpp"

class AddidasShoes : public Shoes
{
	public:
	
		virtual std::string Description();
};

#endif