#ifndef NIKESHOES_HPP
#define NIKESHOES_HPP

#include "Shoes.hpp"

class NikeShoes : public Shoes
{
	public:
	
		virtual std::string Description();
};

#endif