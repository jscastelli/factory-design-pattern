#ifndef ADDIDASSHIRT_HPP
#define ADDIDASSHIRT_HPP

#include "Shirt.hpp"

class AddidasShirt : public Shirt
{
	public:
		
		virtual std::string Description();
};

#endif