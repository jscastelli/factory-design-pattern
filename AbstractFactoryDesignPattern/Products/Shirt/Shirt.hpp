#ifndef SHIRT_HPP
#define SHIRT_HPP

#include "../IClothing.hpp"

class Shirt : public IClothing
{
	public:

		virtual std::string Description() { return "Generic Shirt"; };
};

#endif