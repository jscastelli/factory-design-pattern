#ifndef NIKEPANTS_HPP
#define NIKEPANTS_HPP

#include "Pants.hpp"

class NikePants : public Pants
{
	public:
		virtual std::string Description() override;
};

#endif

