#ifndef ADDIDASPANTS_HPP
#define ADDIDASPANTS_HPP

#include "Pants.hpp"

class AddidasPants : public Pants
{
	public:
		
		virtual std::string Description();
};

#endif
