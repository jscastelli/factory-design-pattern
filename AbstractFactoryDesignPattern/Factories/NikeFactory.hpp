#ifndef NIKEFACTORY_HPP
#define NIKEFACTORY_HPP

#include "IClothingFactory.hpp"

#include "../Products/Shoes/NikeShoes.hpp"
#include "../Products/Pants/NikePants.hpp"
#include "../Products/Shirt/NikeShirt.hpp"

class NikeFactory : public IClothingFactory
{
	public:
		
		virtual Shoes* GetShoes();
		virtual Pants* GetPants();
		virtual Shirt* GetShirt();
};

#endif