#ifndef ADDIDASFACTORY_HPP
#define ADDIDASFACTORY_HPP

#include "IClothingFactory.hpp"
#include "../Products/Shoes/AddidasShoes.hpp"
#include "../Products/Pants/AddidasPants.hpp"
#include "../Products/Shirt/AddidasShirt.hpp"


class AddidasFactory : public IClothingFactory
{
	public:
	
		virtual Shoes* GetShoes();
		virtual Pants* GetPants();
		virtual Shirt* GetShirt();
};

#endif