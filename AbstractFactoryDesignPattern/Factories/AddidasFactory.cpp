#include "AddidasFactory.hpp"

Shoes* AddidasFactory::GetShoes()
{
   return new AddidasShoes();
}

Pants* AddidasFactory::GetPants()
{
   return new AddidasPants();
}

Shirt* AddidasFactory::GetShirt()
{
    return new AddidasShirt();
}
