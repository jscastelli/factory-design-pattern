#include "Factories/NikeFactory.hpp"
#include "Factories/AddidasFactory.hpp"

void DisplayOutfit(Shoes* aShoes, Pants* aPants, Shirt* aShirt);

int main()
{
   IClothingFactory* clothingFactory = nullptr;
   clothingFactory = new NikeFactory();

   Shoes* currentShoes = nullptr;
   Pants* currentPants = nullptr;
   Shirt* currentShirt = nullptr;

   //Nike outfit
   currentPants = clothingFactory->GetPants();
   currentShoes = clothingFactory->GetShoes();
   currentShirt = clothingFactory->GetShirt();

   DisplayOutfit(currentShoes, currentPants, currentShirt);

   delete clothingFactory;
   clothingFactory = nullptr;
   delete currentShoes;
   currentShoes = nullptr;
   delete currentPants;
   currentPants = nullptr;
   delete currentShirt;
   currentShirt = nullptr;

   clothingFactory = new AddidasFactory();

   currentShoes = clothingFactory->GetShoes();
   currentPants = clothingFactory->GetPants();
   currentShirt = clothingFactory->GetShirt();

   DisplayOutfit(currentShoes, currentPants, currentShirt);
}

void DisplayOutfit(Shoes* aShoes, Pants* aPants, Shirt* aShirt)
{
   std::cout << "====Current Outfit====" << std::endl;
   std::cout << "Shoes: " << aShoes->Description() << std::endl;
   std::cout << "Pants: " << aPants->Description() << std::endl;
   std::cout << "Shirt: " << aShirt->Description() << std::endl;
   std::cout << "======================" << std::endl;
}