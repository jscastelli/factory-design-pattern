#ifndef DOG_HPP
#define DOG_HPP

#include "Animal.hpp"

class Dog : public Animal
{
   public:

      Dog();
      void Speak();
};

#endif // !DOG_HPP