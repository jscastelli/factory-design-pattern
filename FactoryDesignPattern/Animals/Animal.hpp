#ifndef ANIMAL_HPP
#define ANIMAL_HPP

#include <iostream>

//Animal base class
class Animal
{
   public:
      
      //All derived animals should override/implement the Speak() method.
      virtual void Speak() {};
};

#endif // !IANIMAL_HPP