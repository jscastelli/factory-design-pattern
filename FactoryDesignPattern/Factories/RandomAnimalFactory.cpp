#include "RandomAnimalFactory.hpp"

#include <cstdlib>

RandomAnimalFactory::RandomAnimalFactory()
{
   //Initialize random number generator based on current time
   srand(time(NULL));
}


Animal* RandomAnimalFactory::CreateObject()
{
   Animal* randomAnimal = nullptr;

   int randomNumber = rand() % 1000000;
   randomNumber = randomNumber % 3;

   switch(randomNumber)
   {
      case 0:
         randomAnimal = new Dog();
         break;
      case 1:
         randomAnimal = new Cat();
         break;
      case 2:
         randomAnimal = new Bird();
         break;
      default:
         std::cout << "ERR: Random number out of range!" << std::endl;
   }

   return randomAnimal;
}

//prints the current count of all the animals
